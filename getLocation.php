<?php
require 'db-config.php';
if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){

$GLOBALS['x1']=$_POST['longitude'];
$GLOBALS['y1']=$_POST['latitude'];

$sql = "SELECT longitude,latitude FROM location WHERE id=1";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $GLOBALS['x2'] = $row["longitude"];
        $GLOBALS['y2'] = $row["latitude"];
    }
} else {
    echo "0 results";
}

$GLOBALS['k']= "K";
echo distance($y1, $x1, $y2, $x2, $k);

}

function distance($y1, $x1, $y2, $x2, $unit) {
$lat1 = $y1;
$lon1 = $x1;
$lat2 = $y2;
$lon2 = $x2;
$theta = $lon1 - $lon2;
$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
$dist = acos($dist);
$dist = rad2deg($dist);
$miles = $dist * 60 * 1.1515;
$unit = strtoupper($unit);
if ($unit == "K") {
return ($miles * 1.609344);
} 
else if ($unit == "N") 
{
return ($miles * 0.8684);
} else 
{
return $miles;
}
}
?>
