var GOOGLE_MAP_KEY ='AIzaSyD0iF23ATnSwyq31_N2fcOYbSG1UqtEFFM';
function ipLookUp() {
    $.ajax('http://ip-api.com/json')
        .then(
            function success(response) {
                console.log('User\'s Location Data is ', response);
                console.log('User\'s Country', response.country);
                getAdress(response.lat, response.lon)
            },

            function fail(data, status) {
                console.log('Request failed.  Returned status of',
                    status);
            }
        );
}

function getAddress(latitude, longitude) {
    $.ajax('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&key=' +
            GOOGLE_MAP_KEY)
        .then(
            function success(response) {
                console.log('User\'s Address Data is ', response)
            },
            function fail(status) {
                console.log('Request failed.  Returned status of',
                    status)
            }
        )
}
if ("geolocation" in navigator) {
    // check if geolocation is supported/enabled on current browser
    navigator.geolocation.getCurrentPosition(
        function success(position) {
            // for when getting location is a success
            console.log('latitude', position.coords.latitude,
                'longitude', position.coords.longitude);
            getAddress(position.coords.latitude,
                position.coords.longitude)
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
     $.ajax({
        type:'POST',
        url:'getLocation.php',
        data:'latitude='+latitude+'&longitude='+longitude,
        success:function(msg){
            console.log(msg);
            if (msg<=20)
            {
                $(".check").attr("disabled", false);
                $(".message").text("User within 20km and your range is: " + msg).css("color","green");
            }
            else
            {
                $(".check").attr("disabled", true);
                $(".message").text("User login only within 20km and your range is: " + msg).css("color","red");

            }

        }
    });

        },
        function error(error_message) {
            // for when getting location results in an error
            console.error('An error has occured while retrieving location', error_message)
            ipLookUp()
        }
    );
}
 else {
    // geolocation is not supported
    // get your location some other way
    console.log('geolocation is not enabled on this browser')
    ipLookUp()
}

function showLocation(position){
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    $.ajax({
        type:'POST',
        url:'getLocation.php',
        data:'latitude='+latitude+'&longitude='+longitude,
        success:function(msg){
            if(msg){
                alert('latitude='+latitude+'&longitude='+longitude);
               $("#location").html(msg);
            }else{
                $("#location").html('Not Available');
            }
        }
    });
}